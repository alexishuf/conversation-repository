package br.ufsc.inf.lapesd.us.agents.conversation_repository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Conversation {
    @JsonIgnore
    private final long id;

    @JsonIgnore
    private ConversationStore store = new ConversationStore();

    public Conversation(long id) {
        this.id = id;
    }

    @JsonIgnore
    public ConversationStore getStore() {
        return store;
    }

    @JsonProperty
    public List<Long> getClientMessageIds() {
        return store.getClientInbox().listMessages();
    }

    @JsonProperty
    public List<Long> getServerMessageIds() {
        return store.getServerInbox().listMessages();
    }

    @JsonProperty
    public long getId() {
        return id;
    }
}
