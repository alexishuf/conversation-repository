package br.ufsc.inf.lapesd.us.agents.conversation_repository.endpoints.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such message")
public class NoSuchMessageException extends RuntimeException {
}
