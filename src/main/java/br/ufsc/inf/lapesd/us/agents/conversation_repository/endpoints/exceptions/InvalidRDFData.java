package br.ufsc.inf.lapesd.us.agents.conversation_repository.endpoints.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "RDF data could not be parsed")
public class InvalidRDFData  extends RuntimeException {
}
