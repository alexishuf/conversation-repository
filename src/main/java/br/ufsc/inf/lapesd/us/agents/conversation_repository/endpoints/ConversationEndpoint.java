package br.ufsc.inf.lapesd.us.agents.conversation_repository.endpoints;

import br.ufsc.inf.lapesd.us.agents.conversation_repository.endpoints.exceptions.InvalidRDFData;
import br.ufsc.inf.lapesd.us.agents.conversation_repository.endpoints.exceptions.NoSuchConversationException;
import br.ufsc.inf.lapesd.us.agents.conversation_repository.endpoints.exceptions.NoSuchMessageException;
import br.ufsc.inf.lapesd.us.agents.conversation_repository.model.Conversation;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.RiotException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;

@RestController
public class ConversationEndpoint {
    private final Logger log = LoggerFactory.getLogger(ConversationEndpoint.class);
    private long nextId = 1;
    private HashMap<Long, Conversation> conversations = new HashMap<>();

    @RequestMapping(value = "/conversations", method = RequestMethod.POST)
    public synchronized  @ResponseBody Conversation createConversation() {
        Conversation conversation = new Conversation(nextId++);
        conversations.put(conversation.getId(), conversation);
        return conversation;
    }

    @RequestMapping(value = "/conversations/{id}", method = RequestMethod.GET)
    public synchronized  @ResponseBody Conversation getConversation(@PathVariable long id) {
        Conversation conversation = conversations.get(id);
        if (conversation == null)
            throw new NoSuchConversationException();
        return conversation;
    }

    @RequestMapping(value = "/conversations/{id}", method = RequestMethod.DELETE)
    public synchronized void deleteConversation(@PathVariable long id) {
        if (!conversations.containsKey(id))
            throw new NoSuchConversationException();
        conversations.remove(id);
    }

    @RequestMapping(value = "/conversations/{convId}/client/{msgId}.ttl", method = RequestMethod.GET,
                    produces = {"text/turtle"})
    public @ResponseBody String getClientMessage(@PathVariable long convId,
                                                 @PathVariable long msgId) {
        Conversation conversation = getConversation(convId);
        Model model = conversation.getStore().getClientInbox().get(msgId);
        if (model == null)
            throw new NoSuchMessageException();
        return modelToString(model, Lang.TURTLE);
    }

    @RequestMapping(value = "/conversations/{convId}/server/{msgId}.ttl", method = RequestMethod.GET,
            produces = {"text/turtle"})
    public @ResponseBody String getServerMessage(@PathVariable long convId,
                                                 @PathVariable long msgId) {
        Conversation conversation = getConversation(convId);
        Model model = conversation.getStore().getServerInbox().get(msgId);
        if (model == null)
            throw new NoSuchMessageException();
        return modelToString(model, Lang.TURTLE);
    }

    @RequestMapping(value = "/conversations/{id}/client", method = RequestMethod.POST,
                    consumes = {"text/turtle"})
    public @ResponseBody Conversation postMessageToClient(@PathVariable long id,
                                                          @RequestBody String rdf) {
        Conversation conversation = getConversation(id);
        Model model = stringToModel(rdf);
        if (model == null)
            throw new InvalidRDFData();
        conversation.getStore().getClientInbox().put(model);
        return conversation;
    }

    @RequestMapping(value = "/conversations/{id}/server", method = RequestMethod.POST,
                    consumes = {"text/turtle"})
    public @ResponseBody Conversation postMessageToServer(@PathVariable long id,
                                                          @RequestBody String rdf) {
        Conversation conversation = getConversation(id);
        Model model = stringToModel(rdf);
        if (model == null)
            throw new InvalidRDFData();
        conversation.getStore().getServerInbox().put(model);
        return conversation;
    }

    @RequestMapping(value = "/conversations/{convId}/client/{msgId}",
                    method = RequestMethod.DELETE)
    public void deleteClientMessage(@PathVariable long convId, @PathVariable long msgId) {
        Conversation conversation = getConversation(convId);
        if (!conversation.getStore().getClientInbox().delete(msgId))
            throw new NoSuchMessageException();
    }

    @RequestMapping(value = "/conversations/{convId}/server/{msgId}",
            method = RequestMethod.DELETE)
    public void deleteServerMessage(@PathVariable long convId, @PathVariable long msgId) {
        Conversation conversation = getConversation(convId);
        if (!conversation.getStore().getServerInbox().delete(msgId))
            throw new NoSuchMessageException();
    }


    private String modelToString(Model model, Lang lang) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            RDFDataMgr.write(out, model, lang);
            return new String(out.toByteArray(), Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException("Unexpected", e);
        }
    }

    private Model stringToModel(String rdf) {
        for (Lang lang : RDFLanguages.getRegisteredLanguages()) {
            Model tmp = ModelFactory.createDefaultModel();
            try {
                RDFDataMgr.read(tmp, IOUtils.toInputStream(rdf), lang);
                return tmp;
            } catch (RiotException ignored) {
                tmp.close();
            }
        }
        return null;
    }

}
