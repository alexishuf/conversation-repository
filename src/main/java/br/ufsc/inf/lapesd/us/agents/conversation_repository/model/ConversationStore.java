package br.ufsc.inf.lapesd.us.agents.conversation_repository.model;


public class ConversationStore {
    private Inbox clientInbox = new Inbox(), serverInbox = new Inbox();

    public Inbox getClientInbox() {
        return clientInbox;
    }

    public Inbox getServerInbox() {
        return serverInbox;
    }
}
