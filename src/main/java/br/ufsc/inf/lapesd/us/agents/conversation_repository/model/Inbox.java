package br.ufsc.inf.lapesd.us.agents.conversation_repository.model;

import org.apache.jena.rdf.model.Model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Inbox {
    private long next = 1;
    private LinkedHashMap<Long, Model> store = new LinkedHashMap<>();

    public synchronized void put(Model message) {
        store.put(next++, message);
    }

    public synchronized List<Long> listMessages() {
        return store.keySet().stream().collect(Collectors.toList());
    }


    public synchronized boolean delete(long id) {
        if (store.containsKey(id)) return false;
        Model model = store.get(id);
        model.close();
        store.remove(id);
        return true;
    }

    public Model get(long id) {
        return store.get(id);
    }
}
